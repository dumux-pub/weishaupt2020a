Summary
=======
This is the DuMuX module containing the code for producing the results of

* [Weishaupt (2020, PhD thesis)](https://elib.uni-stuttgart.de/handle/11682/10949): *Model Concepts for Coupling Free Flow with Porous Medium Flow at the Pore-Network Scale: From Single-Phase Flow to Compositional Non-Isothermal Two-Phase Flow*

* Weishaupt & Helmig (2020, submitted): *A dynamic and fully implicit non-isothermal, two-phase, two-component pore-network model coupled to free flow for the pore-scale description of evaporation processes*


Installation
============

__Dependencies__
This module [requires](https://dune-project.org/doc/installation/)
* a standard c++17 compliant compiler (e.g. gcc or clang)
* CMake
* pkg-config
* MPI (e.g., OpenMPI)
* Suitesparse (install with `sudo apt-get install libsuitesparse-dev` on Debian/Ubuntu systems)

The easiest way to install this module is to create a new folder and to execute the file
[installWeishaupt2020a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/weishaupt2020a/-/raw/master/installWeishaupt2020a.sh)
in this folder.

```bash
mkdir -p Weishaupt2020a && cd Weishaupt2020a
wget -q https://git.iws.uni-stuttgart.de/dumux-pub/weishaupt2020a/-/raw/master/installWeishaupt2020a.sh && chmod +x installWeishaupt2020a.sh
sh ./installWeishaupt2020a.sh
```
To build and run the numerical example of Weishaupt & Helmig (2020, submitted), follow these steps:

```bash
cd weishaupt2020a/build-cmake/appl/micromodel/micromodel_evaporation
make micromodel_evaporation_ni_mpnc
./micromodel_evaporation_ni_mpnc params/square_flowing_atmosphere.input
```
You may use [ParaView](https://www.paraview.org/) to visualize the results by opening the two `.pvd` files. The `.txt` files contain additional data such as the evaporation rate over time.


Installation with Docker
========================

Create a new folder in your favourite location and change into it

```bash
mkdir Weishaupt2020a
cd Weishaupt2020a
```

Download the container startup script
```bash
wget https://git.iws.uni-stuttgart.de/dumux-pub/weishaupt2020a/-/raw/master/docker_weishaupt2020a.sh
```

Open the Docker Container
```bash
bash docker_weishaupt2020a.sh open
```

After the script has run successfully, you may build all executables

```bash
cd weishaupt2020a/build-cmake
make build_tests
```

and you can run them individually. They are located in the build-cmake folder according to the following paths:

- appl/porenetwork/upscaling
- appl/porenetwork/pcswcurve
- appl/micromodel/single_throat_1p/reference
- appl/micromodel/single_throat_1p/coupled
- appl/micromodel/micromodel_evaporation
- appl/micromodel/micromodel_1p/reference
- appl/micromodel/micromodel_1p/coupled
- appl/micromodel/bend_1p


To reproduce the numerical example of Weishaupt & Helmig (2020), one may execute following commands

```bash
cd appl/micromodel/micromodel_evaporation
./micromodel_evaporation_ni_mpnc params/square_flowing_atmosphere.input
```
