// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \ingroup SpatialParameters
 * \brief The base class for spatial parameters for pore network models.
 */
#ifndef DUMUX_PNM_RANDOM_NETWORK_SPATIAL_PARAMS_1P_HH
#define DUMUX_PNM_RANDOM_NETWORK_SPATIAL_PARAMS_1P_HH

#include <dumux/material/spatialparams/porenetwork/porenetwork1p.hh>

namespace Dumux
{

/*!
 * \ingroup SpatialParameters
 */

/**
 * \brief The base class for spatial parameters for pore network models.
 */
template<class GridGeometry, class Scalar, class SinglePhaseTransmissibilityLaw>
class RandomNetWorkSpatialParams : public PNMOnePBaseSpatialParams<GridGeometry, Scalar, SinglePhaseTransmissibilityLaw,
                                                                   RandomNetWorkSpatialParams<GridGeometry, Scalar, SinglePhaseTransmissibilityLaw>>
{
    using ParentType = PNMOnePBaseSpatialParams<GridGeometry, Scalar, SinglePhaseTransmissibilityLaw,
                                                RandomNetWorkSpatialParams<GridGeometry, Scalar, SinglePhaseTransmissibilityLaw>>;

    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

public:
    using PermeabilityType = Scalar;
    using ParentType::ParentType;

    //! Workaround to halve the pore body volume at the coupling interface and to account for the volume of cylindric pores.
    //! Could also be done by adjusting the actual volume in the grid file.
    Scalar porosityAtPos(const GlobalPosition& globalPos) const
    {
        static const Scalar height =  getParamFromGroup<Scalar>("PNM", "Grid.ThroatHeight", 1.0);
        // We use A = pi*r*r (by setting "Circle" as pore body shape),
        // thus multiplying by the height will eventually yield the correct volume of the pore.
        Scalar value = 1.0 * height;

        // use only half of the volume at the coupling boundary
        const auto yMax = this->gridGeometry().bBoxMax()[1];
        if (globalPos[1] > yMax - 1e-10)
            value *= 0.5;

        return value;
    }
};

} // namespace Dumux

#endif
