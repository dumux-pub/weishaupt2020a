// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A test problem for the one-phase pore network model.
 */
#ifndef DUMUX_PNM1P_PROBLEM_HH
#define DUMUX_PNM1P_PROBLEM_HH

// base problem
#include <dumux/porousmediumflow/problem.hh>
// Pore network model
#if USEMPNC
#include <dumux/porenetworkflow/mpnc/model.hh>
#else
#include <dumux/porenetworkflow/2pnc/model.hh>
#endif

#include <dumux/material/fluidsystems/h2oair.hh>

namespace Dumux
{
template <class TypeTag>
class PNMOnePProblem;

namespace Properties
{
namespace TTag {
#if USEMPNC
#if ISOTHERMAL
// Create new type tags
struct PNMTwoPTypeTag { using InheritsFrom = std::tuple<PNMMPNC>; };
#else
struct PNMTwoPTypeTag { using InheritsFrom = std::tuple<PNMMPNCNI>; };
#endif

#else
#if ISOTHERMAL
// Create new type tags
struct PNMTwoPTypeTag { using InheritsFrom = std::tuple<PNMTwoPNC>; };
#else
struct PNMTwoPTypeTag { using InheritsFrom = std::tuple<PNMTwoPNCNI>; };
#endif
#endif
} // end namespace TTag

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::PNMTwoPTypeTag> { using type = Dumux::PNMOnePProblem<TypeTag>; };

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::PNMTwoPTypeTag> { using type = Dune::FoamGrid<1, 2>; };

template<class TypeTag>
struct UseMoles<TypeTag, TTag::PNMTwoPTypeTag> { static constexpr bool value = true; };

template<class TypeTag>
struct ReplaceCompEqIdx<TypeTag, TTag::PNMTwoPTypeTag> { static constexpr int value = 3; };


template<class TypeTag>
struct EnableGridGeometryCache<TypeTag, TTag::PNMTwoPTypeTag> { static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridFluxVariablesCache<TypeTag, TTag::PNMTwoPTypeTag> { static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridVolumeVariablesCache<TypeTag, TTag::PNMTwoPTypeTag> { static constexpr bool value = true; };

#if !USEMPNC
template<class TypeTag>
struct Formulation<TypeTag, TTag::PNMTwoPTypeTag>
{ static constexpr auto value = TwoPFormulation::p1s0; };
#endif

}

template <class TypeTag>
class PNMOnePProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using GridView = GetPropType<TypeTag, Properties::GridView>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::GridGeometry>::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;

    // copy some indices for convenience
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using Labels = GetPropType<TypeTag, Properties::Labels>;

    using Element = typename GridView::template Codim<0>::Entity;
    using Vertex = typename GridView::template Codim<GridView::dimension>::Entity;

    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;

public:
    template<class SpatialParams>
    PNMOnePProblem(std::shared_ptr<const GridGeometry> gridGeometry,
                   std::shared_ptr<SpatialParams> spatialParams,
                   std::shared_ptr<CouplingManager> couplingManager)
    : ParentType(gridGeometry, spatialParams, "PNM"), couplingManager_(couplingManager)
    {
        hasWaterReservoir_ = getParamFromGroup<bool>(this->paramGroup(), "Problem.HasWaterReservoir", false);
        initialSaturation_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.InitialSaturation");
        initialTemperature_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.InitialTemperature", 273.15 + 20.0);
        initialAirInWaterMoleFraction_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.InitialAirInWaterMoleFraction", 1e-5);
        initialPressure_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.InitialPressure", 1e5);
        logfile_.open("logfile_" + this->name() + ".txt");
        logfile_ << std::left << std::setw(20) << std::setfill(' ') << "Time"
                 << std::left << std::setw(20) << std::setfill(' ') << "totalWaterMass"
                 << std::left << std::setw(20) << std::setfill(' ') << "avgSw"
                 << std::left << std::setw(20) << std::setfill(' ') << "evaporationRate"
                 << std::endl;

        getInitial();
    }

    void postTimeStep(const Scalar time, const Scalar massWater, const Scalar avgSw, const Scalar rate)
    {
        std::cout << std::setprecision(15) << "mass of water is: " << massWater << std::endl;

        logfile_ << std::fixed << std::scientific << std::left << std::setw(20) << std::setfill(' ') << time
                                                  << std::left << std::setw(20) << std::setfill(' ') << massWater
                                                  << std::left << std::setw(20) << std::setfill(' ') << avgSw
                                                  << std::left << std::setw(20) << std::setfill(' ') << rate
                                                  << std::endl;
    }


    /*!
     * \brief Return the temperature within the domain in [K].
     *
     */
    Scalar temperature() const
    { return initialTemperature_; }

     /*!
     * \name Boundary conditions
     */
    // \{
    //! Specifies which kind of boundary condition should be used for
    //! which equation for a finite volume on the boundary.
    BoundaryTypes boundaryTypes(const Element& element, const SubControlVolume& scv) const
    {
        BoundaryTypes bcTypes;
        if (couplingManager().isCoupledDof(CouplingManager::lowDimIdx, scv.dofIndex()))
            bcTypes.setAllCouplingNeumann();
        else if (hasWaterReservoir_ && scv.center()[1] < this->gridGeometry().bBoxMin()[1] + 1e-8)
            bcTypes.setAllDirichlet();
        else
            bcTypes.setAllNeumann();

        return bcTypes;
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume.
     *
     * \param values The dirichlet values for the primary variables
     * \param vertex The vertex (pore body) for which the condition is evaluated
     *
     * For this method, the \a values parameter stores primary variables.
     */
    PrimaryVariables dirichlet(const Element &element,
                               const SubControlVolume &scv) const
    {
        return initialConditions_[scv.dofIndex()];
    }


    // \}

    /*!
     * \name Volume terms
     */
    // \{


    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume.
     *
     * This is the method for the case where the source term is
     * potentially solution dependent and requires some quantities that
     * are specific to the fully-implicit method.
     *
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param elemVolVars All volume variables for the element
     * \param scv The sub control volume
     *
     * For this method, the return parameter stores the conserved quantity rate
     * generated or annihilate per volume unit. Positive values mean
     * that the conserved quantity is created, negative ones mean that it vanishes.
     * E.g. for the mass balance that would be a mass rate in \f$ [ kg / (m^3 \cdot s)] \f$.
     */
    template<class ElementVolumeVariables>
    PrimaryVariables source(const Element &element,
                            const FVElementGeometry& fvGeometry,
                            const ElementVolumeVariables& elemVolVars,
                            const SubControlVolume &scv) const
    {
        PrimaryVariables values(0.0);

        const int vIdx =  scv.dofIndex();

        if (couplingManager().isCoupledDof(CouplingManager::lowDimIdx, vIdx))
        {
            const auto tmp = couplingManager().couplingData().massCouplingCondition(element, fvGeometry, elemVolVars, scv);

            for (int i = 0; i < tmp.size(); ++i)
                values[i] = tmp[i];

#if !ISOTHERMAL
            values[Indices::energyEqIdx] = couplingManager().couplingData().energyCouplingCondition(element, fvGeometry, elemVolVars, scv);
#endif
        }

        values /= scv.volume();

        return values;
    }

    // \}

    PrimaryVariables initial(const Vertex& vertex) const
    {
        return initialConditions_[this->gridGeometry().vertexMapper().index(vertex)];
    }


#if USEMPNC
    void getInitial()
    {
        static constexpr auto numPhases = 2;
        static constexpr auto numComponents = 2;
        using FluidState = GetPropType<TypeTag, Properties::FluidState>;
        using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
        using ParameterCache = typename FluidSystem::ParameterCache;
        using PhaseVector = Dune::FieldVector<Scalar, numPhases>;

        static constexpr auto gasPhaseIdx = FluidSystem::gasPhaseIdx;
        static constexpr auto liquidPhaseIdx = FluidSystem::liquidPhaseIdx;
        static constexpr auto fug0Idx = Indices::fug0Idx;
        static constexpr auto s0Idx = Indices::s0Idx;
        static constexpr auto p0Idx = Indices::p0Idx;


        initialConditions_.resize(this->gridGeometry().numDofs());

        auto fvGeometry = localView(this->gridGeometry());
        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            fvGeometry.bind(element);
            for (const auto& scv : scvs(fvGeometry))
            {
                PrimaryVariables values(0.0);
                FluidState fs;

#if !ISOTHERMAL
                values[Indices::temperatureIdx] = initialTemperature_;
#endif

                // set the fluid temperatures
                fs.setTemperature(initialTemperature_);

                // set water saturation
                if (scv.center()[1] > this->gridGeometry().bBoxMax()[1] - 1e-8)
                {
                    static const Scalar initialSaturationTop = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.InitialSaturationTop");
                    fs.setSaturation(liquidPhaseIdx, initialSaturationTop);
                }
                else
                    fs.setSaturation(liquidPhaseIdx, initialSaturation_);

                fs.setSaturation(gasPhaseIdx, 1.0 - fs.saturation(liquidPhaseIdx));
                // set pressure of the gas phase
                fs.setPressure(gasPhaseIdx, initialPressure_);

                const auto& matParams = this->spatialParams().materialLawParams(element, scv, 0.0);
                PhaseVector pc;
                using MaterialLaw = typename ParentType::SpatialParams::MaterialLaw;
                using MPAdapter = MPAdapter<MaterialLaw, numPhases>;

                const int wPhaseIdx = this->spatialParams().template wettingPhase<FluidSystem>(element, scv, 0.0);
                MPAdapter::capillaryPressures(pc, matParams, fs, wPhaseIdx);
                fs.setPressure(liquidPhaseIdx,
                               fs.pressure(gasPhaseIdx) + pc[liquidPhaseIdx] - pc[gasPhaseIdx]);

                // make the fluid state consistent with local thermodynamic
                // equilibrium
                using MiscibleMultiPhaseComposition = Dumux::MiscibleMultiPhaseComposition<Scalar, FluidSystem>;

                ParameterCache paramCache;
                MiscibleMultiPhaseComposition::solve(fs, paramCache);

                ///////////
                // assign the primary variables
                ///////////

                // all N component fugacities
                for (int compIdx = 0; compIdx < numComponents; ++compIdx)
                    values[fug0Idx + compIdx] = fs.fugacity(gasPhaseIdx, compIdx);

                // first M - 1 saturations
                for (int phaseIdx = 0; phaseIdx < numPhases - 1; ++phaseIdx)
                    values[s0Idx + phaseIdx] = fs.saturation(phaseIdx);

                // first pressure
                static constexpr auto pIdx = GetPropType<TypeTag, Properties::VolumeVariables>::pwIsPrimaryVariableIndex;
                values[p0Idx] = fs.pressure(pIdx);
                values.setState(pIdx);

                initialConditions_[scv.dofIndex()] = values;
            }
        }
    }
#else
    void getInitial()
    {
        PrimaryVariables values(0.0);
        values[Indices::pressureIdx] = initialPressure_;

        initialConditions_.resize(this->gridGeometry().numDofs());

        for (const auto& vertex : vertices(this->gridGeometry().gridView()))
        {
            // start with single phase
            if (initialSaturation_ > 1.0 - 1e-15)
            {
                values.setState(Indices::firstPhaseOnly);
                values[Indices::switchIdx] = initialAirInWaterMoleFraction_;
            }
            else // start with two phases
            {
                values[Indices::switchIdx] = initialSaturation_;
                values.setState(Indices::bothPhases);
            }

            if (vertex.geometry().center()[1] > this->gridGeometry().bBoxMax()[1] - 1e-8)
            {
                static const Scalar initialSaturationTop = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.InitialSaturationTop");
                values[Indices::switchIdx] = initialSaturationTop;
                values.setState(Indices::bothPhases);
            }

#if !ISOTHERMAL
            values[Indices::temperatureIdx] = initialTemperature_;
#endif
            initialConditions_[this->gridGeometry().vertexMapper().index(vertex)] = values;
        }
    }
#endif

    /*!
     * \brief Evaluate the initial invasion state of a pore throat
     *
     * Returns true for a invaded throat and false elsewise.
     */
    bool initialInvasionState(const Element &element) const
    { return false; }

    // \}

    //! Get the coupling manager
    const CouplingManager& couplingManager() const
    { return *couplingManager_; }

    bool verbose() const
    { return false; }

private:
    std::shared_ptr<CouplingManager> couplingManager_;
    Scalar initialSaturation_;
    Scalar initialPressure_;
    std::ofstream logfile_;
    bool hasWaterReservoir_;
    Scalar initialTemperature_;
    Scalar initialAirInWaterMoleFraction_;
    std::vector<PrimaryVariables> initialConditions_;

};
} //end namespace

#endif
