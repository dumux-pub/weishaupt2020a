[TimeLoop]
TEnd = 86400e3 # s
DtInitial = 1 # s
MaxTimeStepSize = 300
DtOutput = 100

[PNM.Grid]
LowerLeft = 0.25e-2 0.5e-2
UpperRight = 1.25e-2 1e-2
NumPores = 19 10

ParameterType = lognormal
MeanPoreRadius = 200e-6
StandardDeviationPoreRadius = 0
ParameterRandomNumberSeed = 1
ThroatRadiusN = 0.1

SubstractRadiiFromThroatLength = true
PriorityList = 3 2 1 0
BoundaryFaceMarker = 1 1 1 2
DeletionProbability = 0 0 1 1
RemoveThroatsOnBoundary = 3
PoreLabelsToApplyFactorForVolume = 2
PoreVolumeFactorForLabel = 0.5

ThroatCrossSectionShape = Square
PoreGeometry = Cube

[Stokes.Grid]
LowerLeft = 0 1e-2
UpperRight = 1.5e-2 1.5e-2
UpstreamCells0 = 20
CellsPerThroat = 3
DownstreamCells0 = 20
Cells1 = 30
CoupleOverPoreRadius = true

# GradingY = 1.2

[Stokes.Problem]
Name = stokes
EnableInertiaTerms = true
InitialRelativeHumidity = 0.2
BoundaryRelativeHumidity = 0.2
DeltaP = 1
ConsiderWallFriction = true
UseSlipCondition = true
EnableCoupling = true
Height = 400e-6

OpenTop = false

[PNM.Problem]
Name = pnm
InitialSaturation = 0.99
InitialSaturationTop = 0.99

[Component]
SolidHeatCapacity = 1
SolidDensity = 1
SolidThermalConductivity = 1

[Problem]
Name = evaporation
EnableGravity = false
OnlyDiffusion = false

[Vtk]
AddVelocity = true
WriteFaceData = false
AlwaysWriteOutput = false

[Stokes.Assembly]
NumericDifference.BaseEpsilon = 1e-10

[PNM.Assembly]
NumericDifferenceMethod = 0

[FluxOverSurface]
Verbose = false

[Newton]
MaxRelativeShift = 1e-6
PlausibilityCheck = false
AllowedSaturationChange = 0.3
MaxSteps = 15
RetryTimeStepReductionFactor = 1e-1
EnableChop = true
TemperatureClampValues = 275 300
NumChoppedUpdates = 4

PressureMaxDelta = 1e1
SaturationMaxDelta = 0.1
TemperatureMaxDelta = 0.1
MaxTimeStepDivisions = 20

[InvasionState]
AccuracyCriterion = 0.999

[PrimaryVariableSwitch]
Verbosity = 2

[MPNC]
ThresholdSw = 0.6
