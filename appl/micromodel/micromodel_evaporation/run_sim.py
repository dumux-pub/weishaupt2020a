#!/usr/bin/env python3

import os
import subprocess
import argparse

def run(args):

    executables = {"2p2c":"micromodel_evaporation",
                   "2p2cni":"micromodel_evaporation_ni",
                   "mpnc":"micromodel_evaporation_mpnc",
                   "mpncni":"micromodel_evaporation_ni_mpnc"}

    executable_dir = os.getcwd()
    simulation_dir =  os.path.abspath(executable_dir + "/" + args["path"] + "/" + args["model"])

    print("sim dir is ", simulation_dir)

    if args["createDirectory"] and not os.path.exists(simulation_dir):
        print("creating new directory")
        os.makedirs(simulation_dir)
    elif not args["createDirectory"] and not os.path.exists(simulation_dir):
        raise OSError("Directory does not exist yet! Use --create")
    elif args["createDirectory"] and os.path.exists(simulation_dir):
        raise OSError("Directory already exists! Delete old dir first!")

    if args["parameters"] is None:
        params_file = "params.input"
    else:
        params_file = os.path.realpath(args["parameters"].name)

    command = 'cp ' + ' ' + params_file + ' ' + simulation_dir + ' && '
    command += '' if args["noSlurm"] else 'srun '
    command += executable_dir + "/" + executables[args["model"]] + " " + params_file
    command += " -PNM.Problem.InitialSaturation " + str(args["initialSaturation"]) + " -PNM.Problem.InitialTemperature " + str(args["initalTemperature"] + 273.15)
    command += " -PNM.Grid.StandardDeviationPoreRadius " + str(args["standardDeviation"]) + " -PNM.Grid.ParameterRandomNumberSeed " + str(args["randomSeed"]) + " -PNM.Grid.ThroatCrossSectionShape " + str(args["throatGeometry"])
    command += " -Stokes.Problem.DeltaP " + str(args["deltaP"]) + (" -Stokes.Problem.OpenTop true " if abs(args["deltaP"]) < 1e-15 else " -Stokes.Problem.OpenTop false ")
    command += " -Newton.PlausibilityCheck " + ("true " if (args["model"] == "2p2c" or args["model"] == "2p2cni") else "false")

    command += " > " + args["model"] + ".txt " + " 2> " + args["model"] + ".err &"

    print(command)

    # set environment variables
    my_env = os.environ.copy()
    my_env["OPENBLAS_NUM_THREADS"] = str(args["numThreads"])
    # tmp = "echo $OPENBLAS_NUM_THREADS"

    subprocess.Popen(command, cwd=simulation_dir, shell=True, env=my_env)


parser = argparse.ArgumentParser()
parser.add_argument('-m', '--model', type=str, required=True, help='the model')
parser.add_argument('-iS', '--initialSaturation', type=float, required=True, default=0.99)
parser.add_argument('-iT', '--initalTemperature', type=float, required=False, default=20.0)
parser.add_argument('-dP', '--deltaP', type=float, required=True)
parser.add_argument('-ns', '--noSlurm', action='store_true')
parser.add_argument('-create', '--createDirectory', action='store_true')
parser.add_argument('-p', '--path', type=str, required=False, default="", help='the relative path for the simulation results')
parser.add_argument('-nt', '--numThreads', type=int, required=False, default=2, help='number of threads used by BLAS')
parser.add_argument('-params', '--parameters', type=argparse.FileType('r', encoding='UTF-8'), help='parameter file (relative path)')
parser.add_argument('-tg', '--throatGeometry', type=str, required=False, default='Square', help='throat geometry')
parser.add_argument('-stddev', '--standardDeviation', type=float, required=False, default=0.0, help='pore radius std dev')
parser.add_argument('-rs', '--randomSeed', type=int, required=False, default=1, help='random number seed')
args = vars(parser.parse_args())

run(args)
