import os
import subprocess

def run(case, angle, pressureGradient, inflowBottom, noSlip, useSlurm):
    caseName = str(case)

    problemName = caseName + '_' + str(angle)
    problemName += '_withDp' if pressureGradient else '_noDp'
    problemName += '_withBottomInflow' if inflowBottom else '_noBottomInflow'
    problemName += '_noSlip' if noSlip else ''

    os.system('mkdir ' + problemName)

    wd = os.getcwd()

    command = 'srun ' if useSlurm else ''
    command += './../single_throat_coupled_1p '
    command += './../inputfiles/' + caseName + '.input '
    command += '-PNM.Grid.File ' + wd + '/grids/'  + caseName + '_' + str(angle) + '.dgf '
    command += '-Problem.Name ' + problemName + ' '
    command += '-PNM.Problem.BottomInflow ' + ('1.33333e-14 ' if inflowBottom else '0 ')
    command += '-Stokes.Problem.InletPressure ' + (str(1e-6) + ' ' if pressureGradient else '0 ')
    command += '-Stokes.Problem.UseSlipCondition ' + ('false ' if noSlip else 'true ')
    command += ' > ' + problemName + '.log' + ' 2> ' + problemName + '.err &'

    print command

    subprocess.Popen(command, cwd=wd + '/' + problemName, shell=True)


run(case=100, angle=45, pressureGradient=True, inflowBottom=True, noSlip=True, useSlurm=False)
