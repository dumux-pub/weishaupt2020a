import numpy as np

def writeGrid(throatOpening, yBottom, angle, poreRadius=None):
    filename = str(throatOpening) + "_" + str(abs(angle))

    angle = np.deg2rad(angle)
    # convert to micro meter
    throatOpening *= 1e-6
    yBottom *= 1e-6
    if poreRadius is None:
        poreRadius = throatOpening
    else:
        poreRadius *= 1e-6

    pointTop = np.asarray([0.5e-3, 0.0])
    offSetBottom = abs(yBottom)/np.tan(angle) if abs(angle) > 0.0 else 0.0
    pointBottom = np.asarray([0.5e-3 + offSetBottom, yBottom])

    throatLength = np.linalg.norm(pointTop - pointBottom)
    throatRadius = np.cos(angle) * throatOpening / 2.0

    # write DGF file
    with open(filename + ".dgf", "w") as outputfile:
        outputfile.write("DGF\n")
        outputfile.write("Vertex\n")
        outputfile.write("parameters 2\n") # vertex data
        outputfile.write("{} {} {} {}\n".format(pointBottom[0], pointBottom[1], poreRadius, 2))
        outputfile.write("{} {} {} {}\n".format(pointTop[0], pointTop[1], poreRadius, 3))
        outputfile.write("\n#\n")
        outputfile.write("SIMPLEX\n")
        outputfile.write("parameters 2\n") # cell data
        outputfile.write("{} {} {} {}\n".format(0, 1, throatRadius, throatLength))
        outputfile.write("\n#\n")
        outputfile.write("BOUNDARYDOMAIN\ndefault 1\n#")

    return pointBottom, throatLength, throatRadius

for i in [50, 100, 200, 400]:
    print(writeGrid(i, -200, 0))
    print(writeGrid(i, -200, -45))
