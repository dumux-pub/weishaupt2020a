// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Auxiliary class used to select the elements for the final grid
 */
#ifndef DUMUX_MICROMODEL_ELEMENT_SELECTOR_HH
#define DUMUX_MICROMODEL_ELEMENT_SELECTOR_HH

#include <string>
#include <dune/common/fvector.hh>
#include <dune/geometry/axisalignedcubegeometry.hh>
#include <dune/geometry/multilineargeometry.hh>
#include <dumux/common/geometry/intersectspointgeometry.hh>

namespace Dumux
{

/*!
 * \brief Auxiliary class used to select the elements for the final grid
 */
template <class GridView>
class ElementSelector
{
    using Scalar = typename GridView::ctype;
    static constexpr int coordDim = 2;

    using GlobalPosition = Dune::FieldVector<Scalar, coordDim>;
    using Rectangle = Dune::AxisAlignedCubeGeometry<Scalar, coordDim, coordDim>;
    using Triangle = Dune::MultiLinearGeometry<Scalar, coordDim, coordDim>;

public:
    ElementSelector(const std::string& modelParamGroup = "")
    {
        const Scalar maxX = getParamFromGroup<Scalar>(modelParamGroup, "Grid.MaxX");
        const Scalar maxY = getParamFromGroup<Scalar>(modelParamGroup, "Grid.MaxY");

        const GlobalPosition lowerLeft = GlobalPosition{0.0, 0.0};
        const GlobalPosition upperRight = GlobalPosition{maxX, maxY};

        Rectangle belowInlet(lowerLeft, upperRight);
        rectangles_.push_back(belowInlet);
    }

    //! Select all elements that are not cut-out by the recangles or trinagles
    bool operator() (const auto& element) const
    {
        // make the selector work for 2D and 3D
        const auto center = [&]()
        {
            // for 3D, only consider the x and y component
            const auto tmpCenter = element.geometry().center();
            return GlobalPosition{tmpCenter[0], tmpCenter[1]};
        }();

        for (auto&& rectangle : rectangles_)
        {
            if (intersectsPointGeometry(center, rectangle))
                return false;
        }

        return true;
    }

private:
    std::vector<Rectangle> rectangles_;
};

} //end namespace

#endif
