// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A upscaling problem for the one-phase pore network model.
 */
#ifndef DUMUX_UPSCALING1P_PROBLEM_HH
#define DUMUX_UPSCALING1P_PROBLEM_HH


// base problem
#include <dumux/porousmediumflow/problem.hh>
// Pore network model
#include <dumux/porenetworkflow/1p/model.hh>
// get direct solvers too
#include <dumux/material/components/simpleh2o.hh>
#include <dumux/material/fluidsystems/1pliquid.hh>

#include "pnmspatialparams.hh"

namespace Dumux {

template <class TypeTag>
class UpscalingProblem;

template<class Scalar>
class CouplingTransmissibilityAzzamDullien;

namespace Properties
{
// Create new type tags
namespace TTag {
struct UpscalingProblemTypeTag { using InheritsFrom = std::tuple<PNMOneP>; };
} // end namespace TTag

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::UpscalingProblemTypeTag> { using type = Dumux::UpscalingProblem<TypeTag>; };

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::UpscalingProblemTypeTag>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = FluidSystems::OnePLiquid<Scalar, Dumux::Components::SimpleH2O<Scalar> >;
};

template<class TypeTag>
struct SinglePhaseTransmissibilityLaw<TypeTag, TTag::UpscalingProblemTypeTag>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = TransmissibilityBruus<Scalar>;
};

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::UpscalingProblemTypeTag> { using type = Dune::FoamGrid<1, 3>; };

template<class TypeTag>
struct SpatialParams<TypeTag, TTag::UpscalingProblemTypeTag>
{
private:
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using Transmissibility = GetPropType<TypeTag, Properties::SinglePhaseTransmissibilityLaw>;
public:
    using type = PNMOnePUpscalingSpatialParams<GridGeometry, Transmissibility, Scalar>;
};

}

template <class TypeTag>
class UpscalingProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using GridView = GetPropType<TypeTag, Properties::GridView>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::GridGeometry>::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;

    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using Labels = GetPropType<TypeTag, Properties::Labels>;

    static constexpr int dim = GridView::dimension;
    static constexpr int dimworld = GridView::dimensionworld;

    using Element = typename GridView::template Codim<0>::Entity;
    using Vertex = typename GridView::template Codim<dim>::Entity;

    struct UpscaledData
    {
        Scalar outflowArea;
        Scalar massFlux;
        Scalar vDarcy;
        Scalar K;
    };

public:
    template<class SpatialParams>
    UpscalingProblem(std::shared_ptr<const GridGeometry> gridGeometry, std::shared_ptr<SpatialParams> spatialParams)
        : ParentType(gridGeometry, spatialParams), eps_(1e-7), dirNames_({{"x", "y", "z"}})
    {
        // get some parameters from the input file
        direction_ = 0;
        pressureGradient_ = getParam<Scalar>("Problem.PressureGradient");

        // get the domain's dimensions
        for (int i = 0; i < lenght_.size(); ++i)
            lenght_[i] = this->gridGeometry().bBoxMax()[i] - this->gridGeometry().bBoxMin()[i];

        this->spatialParams().updateBoundaryFactor(direction_);
    }

    /*!
     * \brief Calculate the intrinsic permeability
     * \param massFlux The current mass flux leaving the system
     */
    void doUpscaling(const Scalar massFlux)
    {
        // convert mass to volume flux
        GetPropType<TypeTag, Properties::FluidState> fluidState;
        fluidState.setTemperature(temperature());
        const Scalar pressureAtOutlet = 1e5 - pressureGradient_ * lenght_[direction_];
        fluidState.setPressure(0, pressureAtOutlet);
        Scalar density = FluidSystem::density(fluidState, 0);
        Scalar viscosity = FluidSystem::viscosity(fluidState, 0);
        const auto volumeFlux = massFlux / density;

        auto lenght = lenght_;
        lenght[direction_] = 1.0;
        const auto outflowArea = std::accumulate(lenght.begin(), lenght.end(), 1.0, std::multiplies<Scalar>());
        const auto vDarcy = volumeFlux / outflowArea;
        const auto K = vDarcy / pressureGradient_ * viscosity;
        std::cout << dirNames_[direction_] << "-direction";
        std::cout << ": Area = " << outflowArea;
        std::cout << "; Massflux = " << massFlux << " kg/s";
        std::cout << "; v_Darcy = " << vDarcy << " m/s";
        std::cout << "; K = " << K << " m^2" << std::endl;
        upscaledData_[direction_] = std::move(UpscaledData{outflowArea, massFlux, vDarcy, K});

        if (direction_ == dimworld-1)
        {
            std::cout << "\nNumerical upscaling done:" << std::endl;

            for(int i = 0; i < upscaledData_.size(); ++i)
            {
                std::cout << dirNames_[i] << "-direction";
                std::cout << ": Area = " << upscaledData_[i].outflowArea << " m^2";
                std::cout << "; Massflux = " << upscaledData_[i].massFlux << " kg/s";
                std::cout << "; v_Darcy = " << upscaledData_[i].vDarcy << " m/s";
                std::cout << "; K = " << upscaledData_[i].K << " m^2" << std::endl;
            }
        }
    }

    /*!
     * \brief Return the temperature within the domain in [K].
     *
     */
    Scalar temperature() const
    { return 273.15 + 10; } // 10°C
    // \}

     /*!
     * \name Boundary conditions
     */
    // \{
    //! Specifies which kind of boundary condition should be used for
    //! which equation for a finite volume on the boundary.
    BoundaryTypes boundaryTypes(const Element &element, const SubControlVolume &scv) const
    {
        BoundaryTypes bcTypes;

        const auto pos = scv.dofPosition();
        if (pos[direction_] < this->gridGeometry().bBoxMin()[direction_] + eps_ || pos[direction_] > this->gridGeometry().bBoxMax()[direction_] - eps_)
        {
            bcTypes.setAllDirichlet();
        }
        else // neuman for the remaining boundaries
            bcTypes.setAllNeumann();

        return bcTypes;
    }

        /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume.
     *
     * \param values The dirichlet values for the primary variables
     * \param vertex The vertex (pore body) for which the condition is evaluated
     *
     * For this method, the \a values parameter stores primary variables.
     */
     PrimaryVariables dirichlet(const Element &element,
                                const SubControlVolume &scv) const
     {
         PrimaryVariables values(0.0);

        auto pos = scv.dofPosition();
        if (pos[direction_] < this->gridGeometry().bBoxMin()[direction_] + eps_ )
            values[Indices::pressureIdx] = 1e5;
        else
            values[Indices::pressureIdx] = 1e5 - pressureGradient_ * lenght_[direction_];

        return values;
    }


    // \}

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * For this method, the \a priVars parameter stores primary
     * variables.
     */
    PrimaryVariables initial(const Vertex& vertex) const
    {
        PrimaryVariables values(0.0);

        values[Indices::pressureIdx] = 1e5;
        return values;
    }

    /*!
     * \brief Sets the current direction in which the pressure gradient is applied
     * \param directionIdx The index of the direction (0:x, 1:y, 2:z)
     */
    void setDirection(int directionIdx)
    {
        direction_ = directionIdx;
        this->spatialParams().updateBoundaryFactor(directionIdx);
    }

    // \}

private:
    Scalar eps_;
    int direction_;
    Scalar pressureGradient_;
    std::array<Scalar, dimworld> lenght_;
    std::array<UpscaledData, dimworld> upscaledData_;
    std::array<std::string, 3> dirNames_;
};

} //end namespace

#endif
