// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \ingroup SpatialParameters
 * \brief The base class for spatial parameters for pore network models.
 */
#ifndef DUMUX_PNM_1P_UPSCALING_SPATIAL_PARAMS_HH
#define DUMUX_PNM_1P_UPSCALING_SPATIAL_PARAMS_HH

#include <dumux/material/spatialparams/porenetwork/porenetworkbase.hh>

namespace Dumux
{

/*!
 * \ingroup SpatialParameters
 */

/**
 * \brief The base class for spatial parameters for pore network models.
 */
template<class GridGeometry, class SinglePhaseTransmissibilityLaw, class Scalar>
class PNMOnePUpscalingSpatialParams : public PNMBaseSpatialParams<GridGeometry, Scalar,
                                                         PNMOnePUpscalingSpatialParams<GridGeometry, SinglePhaseTransmissibilityLaw, Scalar>>
{
    using ParentType = PNMBaseSpatialParams<GridGeometry, Scalar,
                                            PNMOnePUpscalingSpatialParams<GridGeometry, SinglePhaseTransmissibilityLaw, Scalar>>;

    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;

public:
    using PermeabilityType = Scalar;

    PNMOnePUpscalingSpatialParams(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry)
    {
        reduceFluxesOnBoundary_ = getParam<bool>("Problem.ReduceFluxesOnBoundary", false);
        factor_.resize(this->gridGeometry().gridView().size(0), 1.0);
    }

    //! Calculate a factor to reduce the fluxes at the edges in order to create a uniform flow field
    void updateBoundaryFactor(const int direction)
    {
        if(!reduceFluxesOnBoundary_)
            return;

        std::fill(factor_.begin(), factor_.end(), 1.0);
        const auto& gridGeometry = this->gridGeometry();

        // Decrease the transmissibility (and thereby the fluxes) by a factor of
        // 0.5 for pores on the boundary and by  a factor of 0.25 for pores in corners.
        // This is important for the coupling with box bulk domains. Otherwise, bulk scvs at the
        // boundaries or at corners would receive too much mass flow.
        // Using this factor, a homogeneous pressure distribution (for symmetrical configurations)
        // can be realized.

        const std::vector<int> allDimensions = {0, 1, 2};
        std::vector<int> otherDimensions;
        std::copy_if(allDimensions.begin(), allDimensions.end(), std::back_inserter(otherDimensions), [&](const int i) { return i != direction; });
        assert(otherDimensions.size() == allDimensions.size() -1);

        for(const auto& element : elements(gridGeometry.gridView()))
        {
            const auto eIdx = gridGeometry.elementMapper().index(element);
            const auto pos = element.geometry().center();
            const auto bBoxMin = gridGeometry.bBoxMin();
            const auto bBoxMax = gridGeometry.bBoxMax();

            const int otherIdx0 = otherDimensions[0];
            const int otherIdx1 = otherDimensions[1];

            // treat pores on the boundary
            if(pos[otherIdx0] < bBoxMin[otherIdx0] + eps_ || pos[otherIdx0] > bBoxMax[otherIdx0] - eps_ ||
               pos[otherIdx1] < bBoxMin[otherIdx1] + eps_ || pos[otherIdx1] > bBoxMax[otherIdx1] - eps_)
               factor_[eIdx] *= 0.5;

            // treat pores at the corners
            if((pos[otherIdx0] < bBoxMin[otherIdx0] + eps_ || pos[otherIdx0] > bBoxMax[otherIdx0] - eps_) &&
               (pos[otherIdx1] < bBoxMin[otherIdx1] + eps_ || pos[otherIdx1] > bBoxMax[otherIdx1] - eps_))
               factor_[eIdx] *= 0.5;
        }
    }

    /*!
    * \brief Returns the transmissibility of a throat
    */
   template<class ElementVolumeVariables, class FluxVariablesCache>
   Scalar transmissibility(const Element& element,
                           const FVElementGeometry& fvGeometry,
                           const SubControlVolumeFace& scvf,
                           const ElementVolumeVariables& elemVolVars,
                           const FluxVariablesCache& fluxVarsCache,
                           const int phaseIdx = 0) const
   {
       const auto eIdx = this->gridGeometry().elementMapper().index(element);
       // forward to specialized function
       return SinglePhaseTransmissibilityLaw::singlePhaseTransmissibility(element, fvGeometry, scvf, fluxVarsCache) * factor_[eIdx];
   }

    Scalar porosityAtPos(const GlobalPosition& globalPos) const
    { return 1.0; }

private:
    const Scalar eps_ = 1e-7;
    bool reduceFluxesOnBoundary_;
    std::vector<Scalar> factor_;

};

} // namespace Dumux

#endif
