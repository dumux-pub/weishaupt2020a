#!/bin/sh

### create a folder for the DUNE and DuMuX modules
### go into the folder and execute this script

if [ -d dune-common ]; then
  echo "error: A directory named dune-common already exists."
  echo "Aborting."
  exit 1
fi

# dune-common
# master # 67fcf531dc19ff8573a1087fa51093c3f597a5f5 # 2019-09-04 09:46:24 +0000 # Ansgar Burchardt
git clone https://gitlab.dune-project.org/core/dune-common.git
cd dune-common
git checkout master
git reset --hard 67fcf531dc19ff8573a1087fa51093c3f597a5f5
cd ..

# dune-geometry
# master # f2d7647239daa0b4aef3fc3d2648fe7a3318bfe4 # 2019-09-03 20:24:58 +0000 # Oliver Sander
git clone https://gitlab.dune-project.org/core/dune-geometry.git
cd dune-geometry
git checkout master
git reset --hard f2d7647239daa0b4aef3fc3d2648fe7a3318bfe4
cd ..

# dune-grid
# master # d91ccd8ca5b049a1dbfa33e7db5a65e51fc4dedf # 2019-08-01 10:30:55 +0000 # Steffen Müthing
git clone https://gitlab.dune-project.org/core/dune-grid.git
cd dune-grid
git checkout master
git reset --hard d91ccd8ca5b049a1dbfa33e7db5a65e51fc4dedf
cd ..

# dune-localfunctions
# master # 154a263118225b2fb38ce77b4150dcc2237caf65 # 2019-09-04 06:32:14 +0000 # Oliver Sander
git clone https://gitlab.dune-project.org/core/dune-localfunctions.git
cd dune-localfunctions
git checkout master
git reset --hard 154a263118225b2fb38ce77b4150dcc2237caf65
cd ..

# dune-istl
# master # 249d1edc2ef6b78a5c09bceb73e4b3036c19b092 # 2019-09-05 12:24:17 +0000 # Oliver Sander
git clone https://gitlab.dune-project.org/core/dune-istl.git
cd dune-istl
git checkout master
git reset --hard 249d1edc2ef6b78a5c09bceb73e4b3036c19b092
cd ..

# dune-foamgrid
# master # 167d88fe0978d9a1a1403f87bb2f6d20bf32b8cd # 2019-10-04 08:16:50 +0000 # Oliver Sander
git clone https://gitlab.dune-project.org/extensions/dune-foamgrid.git
cd dune-foamgrid
git checkout master
git reset --hard 167d88fe0978d9a1a1403f87bb2f6d20bf32b8cd
cd ..

# dune-subgrid
# master # 2103a363f32e8d7b60e66eee7ddecf969f6cf762 # 2019-03-11 09:06:46 +0000 # Lasse Hinrichsen
git clone https://git.imp.fu-berlin.de/agnumpde/dune-subgrid.git
cd dune-subgrid
git checkout master
git reset --hard 2103a363f32e8d7b60e66eee7ddecf969f6cf762
cd ..

# dumux
# fix/mpnc-public-indices-in-volvars # 24439b2d9aea0016cbdb1fdce54708851752e17a # 2019-10-28 09:36:20 +0100 # Kilian Weishaupt
git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git
cd dumux
git checkout fix/mpnc-public-indices-in-volvars
git reset --hard 24439b2d9aea0016cbdb1fdce54708851752e17a
cd ..

# this module
git clone https://git.iws.uni-stuttgart.de/dumux-pub/weishaupt2020a.git

# run dunecontrol
./dune-common/bin/dunecontrol --opts=dumux/cmake.opts all

# clean up
rm installWeishaupt2020a.sh
