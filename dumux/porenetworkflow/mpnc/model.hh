// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Base class for all models which use the one-phase,
 *        fully implicit model.
 *        Adaption of the fully implicit scheme to the one-phase flow model.
 */

#ifndef DUMUX_PNM_MPNC_MODEL_HH
#define DUMUX_PNM_MPNC_MODEL_HH

#include <dumux/common/properties.hh>
#include <dumux/porenetworkflow/properties.hh>

#include <dumux/porousmediumflow/nonisothermal/model.hh>
#include <dumux/porousmediumflow/nonisothermal/indices.hh>
#include <dumux/porousmediumflow/nonisothermal/iofields.hh>

#include <dumux/porousmediumflow/mpnc/model.hh>
#include <dumux/porousmediumflow/compositional/switchableprimaryvariables.hh>
#include <dumux/material/spatialparams/porenetwork/porenetwork2p.hh>
#include <dumux/material/fluidmatrixinteractions/porenetwork/transmissibility1p.hh>
#include <dumux/material/fluidmatrixinteractions/porenetwork/transmissibility2p.hh>

#include <dumux/porenetworkflow/2p/fluxvariablescache.hh>
#include <dumux/porenetworkflow/2p/gridfluxvariablescache.hh>
#include <dumux/porousmediumflow/compositional/localresidual.hh>


#include "iofields.hh"
#include "volumevariables.hh"

namespace Dumux
{
// \{
///////////////////////////////////////////////////////////////////////////
// properties for the isothermal single phase model
///////////////////////////////////////////////////////////////////////////
namespace Properties {

//////////////////////////////////////////////////////////////////
// Type tags
//////////////////////////////////////////////////////////////////

//! The type tags for the implicit single-phase problems
// Create new type tags
namespace TTag {
struct PNMMPNC { using InheritsFrom = std::tuple<PoreNetworkModel, MPNC>; };

//! The type tags for the corresponding non-isothermal problems
struct PNMMPNCNI { using InheritsFrom = std::tuple<PNMMPNC>; };
} // end namespace TTag

//////////////////////////////////////////////////////////////////
// Property tags
//////////////////////////////////////////////////////////////////

//! Set the volume variables property
template<class TypeTag>
struct VolumeVariables<TypeTag, TTag::PNMMPNC>
{
private:
    using PV = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using FSY = GetPropType<TypeTag, Properties::FluidSystem>;
    using FST = GetPropType<TypeTag, Properties::FluidState>;
    using SSY = GetPropType<TypeTag, Properties::SolidSystem>;
    using SST = GetPropType<TypeTag, Properties::SolidState>;
    using MT = GetPropType<TypeTag, Properties::ModelTraits>;
    using PT = typename GetPropType<TypeTag, Properties::SpatialParams>::PermeabilityType;

    static_assert(MT::numFluidPhases() == 2, "Only two-phase flow is supported");

    using Traits = MPNCVolumeVariablesTraits<PV, FSY, FST, SSY, SST, PT, MT>;
public:
    using type = PNMMPNCVolumeVariables<Traits>;
};

//////////////////////////////////////////////////////////////////
// Properties for the isothermal 2pnc model
//////////////////////////////////////////////////////////////////
//! The primary variables vector for the 2pnc model
template<class TypeTag>
struct PrimaryVariables<TypeTag, TTag::PNMMPNC>
{
private:
    using PrimaryVariablesVector = Dune::FieldVector<GetPropType<TypeTag, Properties::Scalar>,
                                                     GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
public:
    using type = SwitchablePrimaryVariables<PrimaryVariablesVector, int>;
};

///////////////////////////////////////////////////////////////////////////
// default property values for the isothermal single phase model
///////////////////////////////////////////////////////////////////////////

//! The spatial parameters to be employed.
//! Use PNMTwoPSpatialParams by default.
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::PNMMPNC>
{
private:
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using Labels = GetPropType<TypeTag, Properties::Labels>;
    using TwoPhaseTransmissibilityLaws = GetPropType<TypeTag, Properties::TwoPhaseTransmissibilityLaws>;
public:
    using type = PNMTwoPSpatialParams<GridGeometry, Scalar, TwoPhaseTransmissibilityLaws, Labels,
                                      getPropValue<TypeTag, Properties::ZeroPc>()>;
};

template<class TypeTag>
struct SinglePhaseTransmissibilityLaw<TypeTag, TTag::PNMMPNC>
{
    using type = TransmissibilityPatzekSilin<GetPropType<TypeTag, Properties::Scalar>>;
};

template<class TypeTag>
struct WettingLayerTransmissibilityLaw<TypeTag, TTag::PNMMPNC>
{
    using type = WettingLayerTransmissibility::RansohoffRadke<GetPropType<TypeTag, Properties::Scalar>>;
};

template<class TypeTag>
struct NonWettingPhaseTransmissibilityLaw<TypeTag, TTag::PNMMPNC>
{
    using type = NonWettingPhaseTransmissibility::BakkeOren<GetPropType<TypeTag, Properties::Scalar>>;
};

template<class TypeTag>
struct TwoPhaseTransmissibilityLaws<TypeTag, TTag::PNMMPNC>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    struct Laws
    {
        using SinglePhaseTransmissibility = GetPropType<TypeTag, Properties::SinglePhaseTransmissibilityLaw>;
        using WettingLayerTransmissibility = GetPropType<TypeTag, Properties::WettingLayerTransmissibilityLaw>;
        using NonWettingPhaseTransmissibility = GetPropType<TypeTag, Properties::NonWettingPhaseTransmissibilityLaw>;
    };
public:
    using type = Laws;
};

template<class TypeTag>
struct IOFields<TypeTag, TTag::PNMMPNC> { using type = PNMMPNCIOFields; };

//! The labels
template<class TypeTag>
struct Labels<TypeTag, TTag::PNMMPNC> { using type = Dumux::Labels; };

//! Set whether a pc of 0 should be applied to pores with sw = 1
template<class TypeTag>
struct ZeroPc<TypeTag, TTag::PNMMPNC> { static constexpr bool value = true; };

template<class TypeTag>
struct EnergyLocalResidual<TypeTag, TTag::PNMMPNC> { using type = Dumux::EnergyLocalResidual<TypeTag> ; };

//! The grid flux variables cache vector class
template<class TypeTag>
struct GridFluxVariablesCache<TypeTag, TTag::PNMMPNC>
{
private:
    static constexpr bool enableCache = getPropValue<TypeTag, Properties::EnableGridFluxVariablesCache>();
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using Labels = GetPropType<TypeTag, Properties::Labels>;
    using FluxVariablesCache = GetPropType<TypeTag, Properties::FluxVariablesCache>;
    using Traits = PNMTwoPDefaultGridFVCTraits<Problem, FluxVariablesCache, Indices, Labels>;
public:
    using type = PNMTwoPGridFluxVariablesCache<Problem, FluxVariablesCache, enableCache, Traits>;
};

//! The flux variables cache
template<class TypeTag>
struct FluxVariablesCache<TypeTag, TTag::PNMMPNC>
{ using type = PNMTwoPFluxVariablesCache<GetPropType<TypeTag, Properties::Scalar>>; };

//////////////////////////////////////////////////////////////////
// Property values for isothermal model required for the general non-isothermal model
//////////////////////////////////////////////////////////////////
template<class TypeTag>
struct ModelTraits<TypeTag, TTag::PNMMPNCNI>
{
private:
private:
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using IsothermalTraits = MPNCModelTraits<FluidSystem::numPhases,
                                             FluidSystem::numComponents,
                                             getPropValue<TypeTag, Properties::PressureFormulation>(),
                                             getPropValue<TypeTag, Properties::UseMoles>(),
                                             getPropValue<TypeTag, Properties::ReplaceCompEqIdx>()>;
public:
    using type = PorousMediumFlowNIModelTraits<IsothermalTraits>;
}; //!< The model traits of the non-isothermal model

//! Set non-isothermal output fields
template<class TypeTag>
struct IOFields<TypeTag, TTag::PNMMPNCNI> { using type = EnergyIOFields<PNMMPNCIOFields>; };


} // end namespace
}


#endif // DUMUX_PNM_MPNC_MODEL_HH
