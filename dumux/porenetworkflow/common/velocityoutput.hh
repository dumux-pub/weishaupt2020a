// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Velocity output for pore-network models
 */
#ifndef DUMUX_PNM_VELOCITYOUTPUT_HH
#define DUMUX_PNM_VELOCITYOUTPUT_HH

#include <dumux/common/properties.hh>
#include <dumux/io/velocityoutput.hh>

namespace Dumux
{

/*!
 * \brief Velocity output for pore-network models
 */
template<class TypeTag>
class PNMVelocityOutput : public VelocityOutput<GetPropType<TypeTag, Properties::GridVariables>>
{
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using ParentType = VelocityOutput<GridVariables>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::GridGeometry>::LocalView;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using ElementVolumeVariables = typename GetPropType<TypeTag, Properties::GridVolumeVariables>::LocalView;
    using FluxVariables = GetPropType<TypeTag, Properties::FluxVariables>;
    using ElementFluxVariablesCache = typename GetPropType<TypeTag, Properties::GridFluxVariablesCache>::LocalView;
    using GridView = GetPropType<TypeTag, Properties::GridView>;

    using Element = typename GridView::template Codim<0>::Entity;

public:
    using VelocityVector = typename ParentType::VelocityVector;

    //! returns the name of the phase for a given index
    std::string phaseName(int phaseIdx) const override { return GetPropType<TypeTag, Properties::FluidSystem>::phaseName(phaseIdx); }

    //! returns the number of phases
    int numFluidPhases() const override { return GetPropType<TypeTag, Properties::ModelTraits>::numFluidPhases(); }

    /*!
     * \brief Constructor initializes the static data with the initial solution.
     *
     * \param problem The problem to be solved
     */
    PNMVelocityOutput(const GridVariables& gridVariables,
                      const SolutionVector& sol)
    :
    gridVariables_(gridVariables),
    sol_(sol)
    {
        velocityOutput_ = getParamFromGroup<bool>(problem_().paramGroup(), "Vtk.AddVelocity");
    }

    bool enableOutput() const override
    { return velocityOutput_; }

    //! Calculate the velocities for the scvs in the element
    //! We assume the local containers to be bound to the complete stencil
    void calculateVelocity(VelocityVector& velocity,
                           const Element& element,
                           const FVElementGeometry& fvGeometry,
                           const ElementVolumeVariables& elemVolVars,
                           const ElementFluxVariablesCache& elemFluxVarsCache,
                           int phaseIdx) const override
    {
        if (!velocityOutput_) return;

        const auto geometry = element.geometry();

        auto tmpVelocity = (geometry.corner(1) - geometry.corner(0));
        tmpVelocity /= tmpVelocity.two_norm();

        const int eIdxGlobal = problem_().gridGeometry().elementMapper().index(element);
        velocity[eIdxGlobal] = 0.0;

        for (auto&& scvf : scvfs(fvGeometry))
        {
            if (scvf.boundary())
                continue;

            // get the volume flux divided by the area of the
            // subcontrolvolume face in the reference element
            // TODO: Divide by extrusion factor!!?
            const Scalar flux = getFlux_(element, fvGeometry, scvf, elemVolVars, elemFluxVarsCache, phaseIdx);

            tmpVelocity *= flux;
            velocity[eIdxGlobal] = tmpVelocity;
        }
    }

private:

    Scalar getFlux_(const Element& element,
                    const FVElementGeometry& fvGeometry,
                    const SubControlVolumeFace& scvf,
                    const ElementVolumeVariables& elemVolVars,
                    const ElementFluxVariablesCache& elemFluxVarsCache,
                    const int phaseIdx) const
    {
        const Scalar localArea = elemFluxVarsCache[scvf].throatCrossSectionalArea(phaseIdx);

        // make sure the phase is actually present (2p)
        if (localArea > 0.0)
        {
            // instantiate the flux variables
            FluxVariables fluxVars;
            fluxVars.init(problem_(), element, fvGeometry, elemVolVars, scvf, elemFluxVarsCache);

            // the upwind term to be used for the volume flux evaluation
            auto upwindTerm = [phaseIdx](const auto& volVars) { return volVars.mobility(phaseIdx); };
            return fluxVars.advectiveFlux(phaseIdx, upwindTerm) / localArea;
        }
        else
            return 0.0;
    }

    const auto& problem_() const { return gridVariables_.curGridVolVars().problem(); }

    bool velocityOutput_;

    const GridVariables& gridVariables_;
    const SolutionVector& sol_;
};

} // end namespace Dumux

#endif
