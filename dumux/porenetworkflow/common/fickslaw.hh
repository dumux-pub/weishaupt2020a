// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief This file contains the data which is required to calculate
 *        diffusive mass fluxes due to molecular diffusion with Fick's law.
 */
#ifndef DUMUX_PNM_FICKS_LAW_HH
#define DUMUX_PNM_FICKS_LAW_HH

#include <dune/common/fvector.hh>
#include <dumux/common/math.hh>
#include <dumux/flux/referencesystemformulation.hh>

namespace Dumux
{

/*!
 * \ingroup PNMFicksLaw
 * \brief Specialization of Fick's Law for the pore-network model.
 */
template <ReferenceSystemFormulation referenceSystem = ReferenceSystemFormulation::massAveraged>
class PNMFicksLaw
{
public:
    //return the reference system
    static constexpr ReferenceSystemFormulation referenceSystemFormulation()
    { return referenceSystem; }

    template<class Problem, class Element, class FVElementGeometry,
             class ElementVolumeVariables, class ElementFluxVariablesCache>
    static auto flux(const Problem& problem,
                     const Element& element,
                     const FVElementGeometry& fvGeometry,
                     const ElementVolumeVariables& elemVolVars,
                     const typename FVElementGeometry::SubControlVolumeFace& scvf,
                     const int phaseIdx,
                     const ElementFluxVariablesCache& elemFluxVarsCache)
    {
        using VolumeVariables = typename ElementVolumeVariables::VolumeVariables;
        using Scalar = typename VolumeVariables::PrimaryVariables::value_type;
        static constexpr auto numComponents = VolumeVariables::numFluidComponents();
        Dune::FieldVector<Scalar, numComponents> componentFlux(0.0);

        // get inside and outside diffusion tensors and calculate the harmonic mean
        const auto& insideVolVars = elemVolVars[scvf.insideScvIdx()];
        const auto& outsideVolVars = elemVolVars[scvf.outsideScvIdx()];

        const auto& fluxVarsCache = elemFluxVarsCache[scvf];

        const Scalar density = 0.5 * (massOrMolarDensity(insideVolVars, referenceSystem, phaseIdx) + massOrMolarDensity(outsideVolVars, referenceSystem, phaseIdx));
        const Scalar throatLength = fluxVarsCache.throatLength();
        const Scalar phaseCrossSectionalArea = fluxVarsCache.throatCrossSectionalArea(phaseIdx);

        for (int compIdx = 0; compIdx < numComponents; compIdx++)
        {
            if(compIdx == phaseIdx)
                continue;

            auto insideD = insideVolVars.diffusionCoefficient(phaseIdx, compIdx);
            auto outsideD = outsideVolVars.diffusionCoefficient(phaseIdx, compIdx);

            // scale by extrusion factor
            insideD *= insideVolVars.extrusionFactor();
            outsideD *= outsideVolVars.extrusionFactor();

            // the resulting averaged diffusion coefficient
            const auto D = Dumux::harmonicMean(insideD, outsideD);

            const Scalar insideMoleFraction = massOrMoleFraction(insideVolVars, referenceSystem, phaseIdx, compIdx);
            const Scalar outsideMoleFraction = massOrMoleFraction(outsideVolVars, referenceSystem, phaseIdx, compIdx);

            componentFlux[compIdx] = density * (insideMoleFraction - outsideMoleFraction) / throatLength * D * phaseCrossSectionalArea;
            componentFlux[phaseIdx] -= componentFlux[compIdx];
        }
        return componentFlux;
    }
};
} // end namespace

#endif
