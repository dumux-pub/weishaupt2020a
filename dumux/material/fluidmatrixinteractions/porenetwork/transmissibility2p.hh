// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Implementation of the transmissibility laws for throats
 */
#ifndef DUMUX_PNM_THROAT_TRANSMISSIBILITY_2P_HH
#define DUMUX_PNM_THROAT_TRANSMISSIBILITY_2P_HH

namespace Dumux {

namespace WettingLayerTransmissibility {


struct CreviceResistanceFactorZhou
{
    /*!
    * \brief Returns the crevice resistance factor used for calculating the w-phase conductance in an invaded pore throat
    *
    * \param alpha The corner half angle
    * \param theta The contact angle
    * \param f The boundary condition factor for the fluid interface (0 = free boundary)
    *
    * Zhou et al. (1997), eq. 19; Singh & Mohanty (2002), eq 8
    */
    template<class Scalar>
    static Scalar beta(const Scalar alpha, const Scalar theta, const Scalar f = 0)
    {
       using std::sin;
       using std::cos;
       using std::tan;
       const Scalar sinAlpha = sin(alpha);
       const Scalar sinSum = sin(alpha + theta);
       const Scalar cosSum = cos(alpha + theta);
       const Scalar phi1 = cosSum*cosSum + cosSum*sinSum*tan(alpha);
       const Scalar phi2 = 1 - theta/(M_PI/2 - alpha);
       const Scalar phi3 = cosSum / cos(alpha);
       const Scalar B = (M_PI/2 - alpha)*tan(alpha);

       Scalar result = 12*sinAlpha*sinAlpha*(1-B)*(1-B)*(phi1 - B*phi2)*(phi3 + f*B*phi2)*(phi3 + f*B*phi2);
       result /= (1-sinAlpha)*(1-sinAlpha)*B*B*(phi1 - B*phi2)*(phi1 - B*phi2)*(phi1 - B*phi2);
       return result;
    }
};


template<class Scalar, class Beta = CreviceResistanceFactorZhou>
struct RansohoffRadke
{
    using CreviceResistanceFactor = Beta;

    /*!
    * \brief Returns the integral conductivity of all wetting layers occupying the corners of a throat
    *
    * \param problem The problem
    * \param element The element
    * \param ElementVolumeVariables The element volume variables
    */
    template<class Element, class FVElementGeometry, class FluxVariablesCache>
    static Scalar transmissibility(const Element& element,
                                   const FVElementGeometry& fvGeometry,
                                   const typename FVElementGeometry::SubControlVolumeFace& scvf,
                                   const FluxVariablesCache& fluxVarsCache)
    {
        const Scalar throatLength = fluxVarsCache.throatLength();
        const Scalar rC = fluxVarsCache.curvatureRadius();

        const auto eIdx = fvGeometry.gridGeometry().elementMapper().index(element);
        const auto numCorners = fvGeometry.gridGeometry().numCorners(eIdx);

        // treat the wetting film layer in each corner of the throat individually (might have different corner half-angle and beta)
        Scalar result = 0.0;
        for (int i = 0; i < numCorners; ++i)
            result += fluxVarsCache.wettingLayerCrossSectionalArea(i) * rC*rC / (throatLength*fluxVarsCache.beta(i));

        assert(!std::isnan(result));
        return result;
    }
};
} // end namespace WettingLayerTransmissibility

namespace NonWettingPhaseTransmissibility {

//! TODO: evalute and maybe remove
template<class Scalar>
struct JoeakerNiasar
{
    template<class Element, class FVElementGeometry, class FluxVariablesCache>
    static Scalar transmissibility(const Element& element,
                                   const FVElementGeometry& fvGeometry,
                                   const typename FVElementGeometry::SubControlVolumeFace& scvf,
                                   const FluxVariablesCache& fluxVarsCache)
    {
        // Joeakar-Niasar (2011), only square throats, can lead to negative values under the square root, maybe a typo in the paper?
        using std::sqrt;
        const Scalar rC = fluxVarsCache.curvatureRadius();
        const Scalar inscribedRadius = fluxVarsCache.throatRadius();
        const Scalar throatLength = fluxVarsCache.throatLength();
        const Scalar rEff = 0.5*(sqrt((inscribedRadius*inscribedRadius-(4-M_PI)*rC*rC)/M_PI)+inscribedRadius);
        const Scalar result = M_PI/(8*throatLength) * rEff*rEff*rEff*rEff; // Mogensen et al. (1999), does not really revover the single phase value
        assert(!std::isnan(result));
        return result;
    }
};

//! TODO: evalute and maybe remove
template<class Scalar>
struct Mogensen
{
    template<class Element, class FVElementGeometry, class FluxVariablesCache>
    static Scalar transmissibility(const Element& element,
                                   const FVElementGeometry& fvGeometry,
                                   const typename FVElementGeometry::SubControlVolumeFace& scvf,
                                   const FluxVariablesCache& fluxVarsCache)
    {
        // Mogensen et al. (1999), does not really revover the single phase value
        using std::sqrt;
        const Scalar throatLength = fluxVarsCache.throatLength();
        const auto nPhaseIdx = fluxVarsCache.nPhaseIdx();
        const Scalar aNw = fluxVarsCache.throatCrossSectionalArea(nPhaseIdx);
        const Scalar rEff = 0.5*(sqrt(aNw / M_PI) + fluxVarsCache.throatRadius());
        const Scalar result = M_PI/(8*throatLength) * rEff*rEff*rEff*rEff;
        assert(!std::isnan(result));
        return result;
    }
};

//! TODO: evalute and maybe remove
template<class Scalar, class SinglePhaseTransmissibilityLaw>
struct Valvatne
{
    template<class Element, class FVElementGeometry, class FluxVariablesCache>
    static Scalar transmissibility(const Element& element,
                                   const FVElementGeometry& fvGeometry,
                                   const typename FVElementGeometry::SubControlVolumeFace& scvf,
                                   const FluxVariablesCache& fluxVarsCache)
    {
        // Tora et al. (2012), also does not fully recover single-phase value, but is closer
        using std::sqrt;
        const auto nPhaseIdx = fluxVarsCache.nPhaseIdx();
        const Scalar aNw = fluxVarsCache.throatCrossSectionalArea(nPhaseIdx);
        const Scalar aTot = fluxVarsCache.throatCrossSectionalArea();

        const Scalar result = SinglePhaseTransmissibilityLaw::singlePhaseTransmissibility(element, fvGeometry, scvf, fluxVarsCache)
                              * aNw / aTot;

        assert(!std::isnan(result));
        return result;
    }
};

template<class Scalar>
struct BakkeOren
{
    /*!
    * \brief Returns the conductivity of a throat.
    *
    * See Bakke & Oren (1997), eq. 9
    */
    template<class Element, class FVElementGeometry, class FluxVariablesCache>
    static Scalar transmissibility(const Element& element,
                                   const FVElementGeometry& fvGeometry,
                                   const typename FVElementGeometry::SubControlVolumeFace& scvf,
                                   const FluxVariablesCache& fluxVarsCache)
    {
        // Tora et al. (2012), quite close for single-phase value of square
        using std::sqrt;
        const Scalar throatLength = fluxVarsCache.throatLength();
        const auto nPhaseIdx = fluxVarsCache.nPhaseIdx();
        const Scalar aNw = fluxVarsCache.throatCrossSectionalArea(nPhaseIdx);
        const Scalar rEff = 0.5*(sqrt(aNw / M_PI) + fluxVarsCache.throatRadius());
        const Scalar result = rEff*rEff*aNw / (8.0*throatLength);
        assert(!std::isnan(result));
        return result;
    }
};
} // end namespace NonWettingPhaseTransmissibility
} // end namespace Dumux

#endif // DUMUX_PNM_THROAT_TRANSMISSIBILITY_2P_HH
