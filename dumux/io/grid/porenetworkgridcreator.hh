// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Provides a grid creator builds a pore network grid from a host grid
 */
#ifndef DUMUX_PORE_GRID_CREATOR_HH
#define DUMUX_PORE_GRID_CREATOR_HH

#include <sstream>
#include <random>

#include <dune/common/classname.hh>
#include <dune/common/exceptions.hh>
#include <dune/common/timer.hh>
#include <dune/geometry/referenceelements.hh>
#include <dune/grid/yaspgrid.hh>
#include <dune/grid/io/file/vtk.hh>

// FoamGrid specific includes
#if HAVE_DUNE_FOAMGRID
#include <dune/foamgrid/foamgrid.hh>
#include <dune/foamgrid/dgffoam.hh>
#else
static_assert(false, "dune-foamgrid required!");
#endif

#include <dumux/common/parameters.hh>
#include <dumux/io/grid/gridmanager_yasp.hh>

#include "griddata.hh"

#include <dune/common/version.hh>

namespace Dumux {

/*!
 * \brief Write foam-grid to dgf file
 */
template<class GridView, class GridData>
inline static void writeDgf(const std::string& fileName, const GridView& gridView, const GridData& gridData)
{
    const auto someElement = *(elements(gridView).begin());
    const auto someVertex = *(vertices(gridView).begin());
    const auto numVertexParams = gridData.parameters(someVertex).size();
    const auto numElementParams = gridData.parameters(someElement).size();

    std::ofstream dgfFile;
    dgfFile.open(fileName);
    dgfFile << "DGF\nVertex % Coordinates, volumes and boundary flags of the pore bodies\nparameters " << numVertexParams << "\n";

    for(const auto& vertex : vertices(gridView))
    {
        const auto pos = vertex.geometry().center();
        const auto& params = gridData.parameters(vertex);

        dgfFile << pos << " ";

        for(int i = 0; i < params.size(); ++i)
        {
            dgfFile << params[i];

            if(i < params.size() - 1)
                dgfFile << " ";
        }

        dgfFile << std::endl;
    }

    dgfFile << "#\nSIMPLEX % Connections of the pore bodies (pore throats)\nparameters " << numElementParams << "\n";

    for(const auto& element : elements(gridView))
    {
        dgfFile << gridView.indexSet().subIndex(element, 0, 1) << " ";
        dgfFile << gridView.indexSet().subIndex(element, 1, 1) << " ";

        const auto& params = gridData.parameters(element);

        for(int i = 0; i < params.size(); ++i)
        {
            dgfFile << params[i];

            if(i < params.size() - 1)
                dgfFile << " ";
        }

        dgfFile << std::endl;
    }

    dgfFile << "#";
    dgfFile.close();
}

template<int dimWorld, class GData = Dumux::PoreNetworkGridData<Dune::FoamGrid<1, dimWorld>>>
class PoreNetworkGridCreator
{
    using GridType = Dune::FoamGrid<1, dimWorld>;
    using Element = typename GridType::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using CoordScalar = typename GridType::ctype;

    using ReferenceElements = typename Dune::ReferenceElements<CoordScalar, dimWorld>;

public:
    using Grid = GridType;
    using GridData = GData;

    void init(const std::string& paramGroup = "")
    {
        Dune::Timer timer;

        paramGroup_ = paramGroup;
        // First try to create it from a DGF file in GridParameterGroup.File
        if (hasParamInGroup(paramGroup, "Grid.File"))
        {
            makeGridFromDgfFile(getParamFromGroup<std::string>(paramGroup, "Grid.File"));
            loadBalance();
        }
        else // no grid file found
        {
            makeGridFromStructuredLattice();
            loadBalance();
        }

        // write the final grid to a dgf file, if desired
        if(getParamFromGroup<bool>(paramGroup_, "Grid.WriteDgfFile", false))
        {
            const auto defaultName = enableDgfGridPointer_ ? "pnm-grid-from-dgf.dgf" : "pnm-grid-from-factory.dgf";
            const auto fileName = getParamFromGroup<std::string>(paramGroup, "Grid.DgfName", defaultName);
            writeDgf(fileName, grid().leafGridView(), *gridData_);
        }

        timer.stop();

        const auto gridType = enableDgfGridPointer_ ?  "grid from dgf file" : "generic grid from structured lattice";

        std::cout << "Creating "  << gridType << " with " << grid().leafGridView().size(0) << " elements and "
                  << grid().leafGridView().size(1) << " vertices took " << timer.elapsed() << " seconds." << std::endl;
    }

    /*!
     * \brief Make a grid from a DGF file.
     */
    void makeGridFromDgfFile(const std::string& fileName)
    {
        // We found a file in the input file...does it have a supported extension?
        const std::string extension = getFileExtension(fileName);
        if(extension != "dgf")
            DUNE_THROW(Dune::IOError, "Grid type " << Dune::className<Grid>() << " only supports DGF (*.dgf) but the specified filename has extension: *."<< extension);

        enableDgfGridPointer_ = true;
        dgfGridPtr() = Dune::GridPtr<Grid>(fileName.c_str(), Dune::MPIHelper::getCommunicator());
        gridData_ = std::make_shared<GridData>(dgfGridPtr_, paramGroup_);

        if(getParamFromGroup<bool>(paramGroup_, "Grid.Sanitize", false))
            sanitizeGrid();
    }

    /*!
     * \brief Make a grid based on a structured lattice which allows to randomly delete elements based on Raoof et al. 2009
     */
    void makeGridFromStructuredLattice()
    {
        try // try to create a one-dimensional grid first ...
        {
            const auto lowerLeft = getParamFromGroup<GlobalPosition>(paramGroup_, "Grid.LowerLeft", GlobalPosition(0.0));
            const auto upperRight = getParamFromGroup<GlobalPosition>(paramGroup_, "Grid.UpperRight");
            const auto numPores = getParamFromGroup<unsigned int>(paramGroup_, "Grid.NumPores");
            const auto cells = numPores-1;

            // make the grid (structured interval grid in dimworld space)
            Dune::GridFactory<Grid> factory;

            constexpr auto geomType = Dune::GeometryTypes::line;

            // create a step vector
            GlobalPosition step = upperRight;
            step -= lowerLeft, step /= cells;

            // create the vertices
            GlobalPosition globalPos = lowerLeft;
            for (unsigned int vIdx = 0; vIdx <= cells; vIdx++, globalPos += step)
                factory.insertVertex(globalPos);

            // create the cells
            for(unsigned int eIdx = 0; eIdx < cells; eIdx++)
                factory.insertElement(geomType, {eIdx, eIdx+1});

            gridPtr() = std::shared_ptr<Grid>(factory.createGrid());
            gridData_ = std::make_shared<GridData>(gridPtr_, paramGroup_);
            gridData_->assignParameters();
            return;
        }
        catch(Dune::RangeError& e) { }

        // ... or construct a 2D or 3D grid
        std::array<std::vector<int>, dimWorld> cells;
        std::array<std::vector<CoordScalar>, dimWorld> positions;
        std::array<std::vector<CoordScalar>, dimWorld> grading;

        // try to get the pore positions explicitly ...
        for (int i = 0; i < dimWorld; ++i)
            positions[i] = getParamFromGroup<std::vector<CoordScalar>>(paramGroup_, "Grid.Positions" + std::to_string(i), std::vector<CoordScalar>{});
        if (std::none_of(positions.begin(), positions.end(), [&](auto& v){ return v.empty(); }))
        {
            for (int i = 0; i < dimWorld; ++i)
            {
                cells[i].resize(positions[i].size()-1, 1.0);
                grading[i].resize(positions[i].size()-1, 1.0);
            }
        }
        else // .. or calculate positions from input data
        {
            const auto lowerLeft = getParamFromGroup<GlobalPosition>(paramGroup_, "Grid.LowerLeft", GlobalPosition(0.0));
            const auto upperRight = getParamFromGroup<GlobalPosition>(paramGroup_, "Grid.UpperRight");
            const auto numPores = getParamFromGroup<std::array<unsigned int, dimWorld>>(paramGroup_, "Grid.NumPores");
            for (int i = 0; i < dimWorld; ++i)
            {
                positions[i].push_back(lowerLeft[i]);
                positions[i].push_back(upperRight[i]);
                cells[i].push_back(numPores[i] - 1);
                grading[i].resize(positions[i].size()-1, 1.0);
                grading[i] = getParamFromGroup<std::vector<CoordScalar>>(paramGroup_, "Grid.Grading" + std::to_string(i), grading[i]);
            }
        }

        // get the lower left position
        const GlobalPosition lowerLeft = [&]()
        {
            GlobalPosition result;
            for (int i = 0; i < dimWorld; ++i)
                result[i] = positions[i][0];
            return result;
        }();

        // get the upper right position
        const GlobalPosition upperRight = [&]()
        {
            GlobalPosition result;
            for (int i = 0; i < dimWorld; ++i)
                result[i] = positions[i].back();
            return result;
        }();

        // create the host grid
        using HostGrid = Dune::YaspGrid<dimWorld, Dune::TensorProductCoordinates<CoordScalar, dimWorld>>;
        using HastGridManager = GridManager<HostGrid>;
        HastGridManager hostGridManager;
        hostGridManager.init(positions, cells, grading, paramGroup_);
        auto hostGridView = hostGridManager.grid().leafGridView();

        // set probabilities for deleting a certain direction (for creating anisotropy)
        static constexpr auto numDirections = (dimWorld < 3) ? 4 : 13;
        std::array<double, numDirections> directionProbability;

        // get user input or print out help message explaining correct usage
        if(hasParamInGroup(paramGroup_, "Grid.DeletionProbability"))
        {
            try { directionProbability = getParamFromGroup<decltype(directionProbability)>(paramGroup_, "Grid.DeletionProbability"); }
            catch(Dumux::ParameterException &e)
            {
                // define directions (to be used for user specified anisotropy)
                Dune::FieldVector<std::string, numDirections> directions;
                if(dimWorld < 3) // 2D
                {
                    // x, y
                    directions[0] = "1: (1, 0)\n";
                    directions[1] = "2: (0, 1)\n";
                    // diagonals through cell midpoint
                    directions[2] = "3: (1, 1)\n";
                    directions[3] = "4: (1, -1)\n";
                }
                else // 3D
                {
                    // x, y, z
                    directions[0] = " 1: (1, 0, 0)\n";
                    directions[1] = "2: (0, 1, 0)\n";
                    directions[2] = "3: (0, 0, 1)\n";
                    //face diagonals
                    directions[3] = "4: (1, 1, 0)\n";
                    directions[4] = "5: (1, -1, 0)\n";
                    directions[5] = "6: (1, 0, 1)\n";
                    directions[6] = "7: (1, 0, -1)\n";
                    directions[7] = "8: (0, 1, 1)\n";
                    directions[8] = "9: (0, 1, -1)\n";
                    // diagonals through cell midpoint
                    directions[9] = "10: (1, 1, 1)\n";
                    directions[10] = "11: (1, 1, -1)\n";
                    directions[11] = "12: (-1, 1, 1)\n";
                    directions[12] = "13: (-1, -1, 1)\n";
                }
                DUNE_THROW(Dumux::ParameterException, "You must specifiy probabilities for all directions (" << numDirections << ") \n" << directions << "\nExample (3D):\n\n"
                << "DeletionProbability = 0.5 0.5 0 0 0 0 0 0 0 0 0 0 0 \n\n"
                << "deletes approximately 50% of all throats in x and y direction, while no deletion in any other direction takes place.\n" );
            }
        }
        else
            directionProbability.fill(0.0);

        // convenience option to delete all diagonal throats
        if (getParamFromGroup<bool>(paramGroup_, "Grid.RegularLattice", false))
        {
            directionProbability.fill(1.0);
            for (int i = 0; i < dimWorld; ++i)
                directionProbability[i] = 0.0;
        }

        // extract the necessary information from the host grid
        std::vector<bool> vertexInserted(hostGridView.size(dimWorld), false);
        std::vector<unsigned int> hostVertexIdxToVertexIdx(hostGridView.size(dimWorld));
        std::vector<unsigned int> edgeInserted(hostGridView.size(dimWorld-1), false);
        std::vector<unsigned int> faceInserted(hostGridView.size(dimWorld-2), false);

        // new vertices and elements
        std::vector<GlobalPosition> vertexSet;
        std::vector<std::pair<unsigned int, unsigned int>> elementSet;

        // prepare random number generation for deletion of elements
        std::mt19937 generator;

        // allow to specify a seed to get reproducible results
        if(hasParamInGroup(paramGroup_, "Grid.DeletionRandomNumberSeed"))
        {
            const auto seed = getParamFromGroup<unsigned int>(paramGroup_, "Grid.DeletionRandomNumberSeed");
            generator.seed(seed);
        }
        else
        {
            std::random_device rd;
            generator.seed(rd());
        }

        std::uniform_real_distribution<> uniformdist(0, 1);

        // compute elements and vertexSet
        for(const auto& element : elements(hostGridView))
        {
            const auto& geometry = element.geometry();
            const auto& refElement = ReferenceElements::general(geometry.type());

            // lambda function for setting vertices and elements
            // will be used both for setting elements on the hostgrid element's edges and diagonals
            auto setVerticesAndElements = [&](unsigned int vIdxLocal0, unsigned int vIdxLocal1, double directionProbability)
            {
                // skip the element if the random number is below a threshold
                if(uniformdist(generator) < directionProbability)
                    return;

                // get global vertex indices w.r.t. host grid
                using HostVertexMapper = Dune::MultipleCodimMultipleGeomTypeMapper<typename HostGrid::LeafGridView>;
                HostVertexMapper vertexMapper(hostGridView, Dune::mcmgVertexLayout());
                const auto vIdxGlobal0 = vertexMapper.subIndex(element, vIdxLocal0, dimWorld);
                const auto vIdxGlobal1 = vertexMapper.subIndex(element, vIdxLocal1, dimWorld);

                // delete elements whose center points lie on the boudary, if specified
                bool skipElement = false;
                const auto removeThroatsOnBoundary = getParamFromGroup<std::vector<unsigned int>>(paramGroup_,
                                                                                                  "Grid.RemoveThroatsOnBoundary",
                                                                                                  std::vector<unsigned int>());

                const bool onlyXYplane = (dimWorld == 3) ? getParamFromGroup<bool>(paramGroup_, "Grid.UseOnlyXYPlane", false)
                                                         : false;

                if(!removeThroatsOnBoundary.empty() || onlyXYplane)
                {
                    const auto& pos0 = geometry.corner(vIdxLocal0);
                    const auto& pos1 = geometry.corner(vIdxLocal1);
                    auto center = (pos1 - pos0);
                    center *= 0.5;
                    center += pos0;

                    const auto eps = 1e-5*geometry.volume(); // adaptive eps

                    if(onlyXYplane)
                    {
                        if(std::find(removeThroatsOnBoundary.begin(), removeThroatsOnBoundary.end(), 4) != removeThroatsOnBoundary.end())
                            DUNE_THROW(Dune::InvalidStateException, "Throats on x-y plane (boundary 4) must not be removed if Grid.UseOnlyXYPlane is set true");

                        if(center[dimWorld-1]> lowerLeft[dimWorld-1] + eps)
                        {
                            // std::cout << "skipping " << center << ", center[2] " << center[2] << ", lowerLeft[2] " << lowerLeft[2] << ", eps "  << eps <<     std::endl;
                            skipElement = true;
                        }
                    }



                    for(auto i : removeThroatsOnBoundary)
                    {
                        if(skipElement)
                            continue;

                        switch(i)
                        {
                            case 0: skipElement = center[0] < lowerLeft[0] + eps; break;
                            case 1: skipElement = center[0] > upperRight[0] - eps; break;
                            case 2: skipElement = center[1] < lowerLeft[1] + eps; break;
                            case 3: skipElement = center[1] > upperRight[1] - eps; break;
                            case 4: skipElement = center[2] < lowerLeft[2] + eps; break;
                            case 5: skipElement = center[2] > upperRight[2] - eps; break;
                        }
                    }
                }

                if(skipElement)
                    return;

                // map from the host grid to the new grid and insert vertices and elements
                unsigned int newVertexIdx0, newVertexIdx1;

                if(vertexInserted[vIdxGlobal0])
                    newVertexIdx0 = hostVertexIdxToVertexIdx[vIdxGlobal0];
                else
                {
                    vertexInserted[vIdxGlobal0] = true;
                    newVertexIdx0 = vertexSet.size();
                    hostVertexIdxToVertexIdx[vIdxGlobal0] = newVertexIdx0;
                    const auto& corner0 = geometry.corner(vIdxLocal0);
                    vertexSet.push_back(corner0);
                }

                if(vertexInserted[vIdxGlobal1])
                    newVertexIdx1 = hostVertexIdxToVertexIdx[vIdxGlobal1];
                else
                {
                    vertexInserted[vIdxGlobal1] = true;
                    newVertexIdx1 = vertexSet.size();
                    hostVertexIdxToVertexIdx[vIdxGlobal1] = newVertexIdx1;
                    const auto& corner1 = geometry.corner(vIdxLocal1);
                    vertexSet.push_back(corner1);
                }

                elementSet.emplace_back(newVertexIdx0, newVertexIdx1);
            };

            // go over all edges and add them as elements if they passed all the tests
            for (unsigned int edgeIdx = 0; edgeIdx < element.subEntities(dimWorld-1); ++edgeIdx)
            {
                const auto vIdxLocal0 = refElement.subEntity(edgeIdx, dimWorld-1, 0, dimWorld);
                const auto vIdxLocal1 = refElement.subEntity(edgeIdx, dimWorld-1, 1, dimWorld);
                const auto edgeIdxGlobal = hostGridView.indexSet().subIndex(element, edgeIdx, dimWorld-1);

                // edge already checked?
                if(edgeInserted[edgeIdxGlobal])
                    continue;
                else
                    edgeInserted[edgeIdxGlobal] = true;

                // pruning (to create anisotropy)
                // assign numbers to different spatial directions
                unsigned int directionNumber(0);

                if(dimWorld == 2 ) // 2D
                {
                    if(edgeIdx < 2) // y-direction
                        directionNumber = 0;
                    else // x-direction
                        directionNumber = 1;
                }
                else // 3D
                {
                    if(edgeIdx < 4) // z-direction
                        directionNumber = 2;
                    else if(edgeIdx == 4 || edgeIdx == 5 || edgeIdx == 8 || edgeIdx == 9) // y-direction
                        directionNumber = 1;
                    else if(edgeIdx == 6 || edgeIdx == 7 || edgeIdx == 10 || edgeIdx == 11) // x-direction
                        directionNumber = 0;
                }
                setVerticesAndElements(vIdxLocal0,  vIdxLocal1, directionProbability[directionNumber]);
            }

            // add diagonals manually:
            std::vector<std::array<unsigned int, 3>> diagonalVertices;
            if(dimWorld == 3) // 3D
            {
                //set diagonals on host grid element faces
                for (unsigned int faceIdx = 0; faceIdx < element.subEntities(dimWorld-2); ++faceIdx)
                {
                    const unsigned int faceIdxGlobal = hostGridView.indexSet().subIndex(element, faceIdx, dimWorld-2);
                    // face already checked?
                    if(faceInserted[faceIdxGlobal])
                        continue;
                    else
                        faceInserted[faceIdxGlobal] = true;

                    // get local vertex indices
                    std::array<unsigned int, 4> vIdxLocal;
                    std::array<unsigned int, 4> vIdxGlobal;
                    for(int i = 0; i < 4; i++)
                    {
                        vIdxLocal[i]  = refElement.subEntity(faceIdx, dimWorld-2, i, dimWorld);
                        vIdxGlobal[i] = hostGridView.indexSet().subIndex(element, vIdxLocal[i], dimWorld);
                    }

                    // pruning (to create anisotropy)
                    // assign numbers to different spatial directions
                    unsigned int directionNumber1, directionNumber2;
                    if(faceIdx < 2)
                    {
                        directionNumber1 = 8;
                        directionNumber2 = 7;
                    }
                    else if(faceIdx < 4)
                    {
                        directionNumber1 = 6;
                        directionNumber2 = 5;
                    }
                    else
                    {
                        directionNumber1 = 4;
                        directionNumber2 = 3;
                    }
                    diagonalVertices.push_back({{vIdxLocal[1],vIdxLocal[2], directionNumber1}});
                    diagonalVertices.push_back({{vIdxLocal[0], vIdxLocal[3], directionNumber2}});
                }
                diagonalVertices.push_back({{0, 7, 9}});
                diagonalVertices.push_back({{3, 4, 10}});
                diagonalVertices.push_back({{1, 6, 11}});
                diagonalVertices.push_back({{2, 5, 12}});
            }

            if(dimWorld == 2) // 2D
            {
                diagonalVertices.push_back({{0,3,2}});
                diagonalVertices.push_back({{1,2,3}});
            }

            // insert all diagonals into the pore network grid
            for (const auto& i : diagonalVertices)
            {
                const auto vIdxLocal0 = i[0];
                const auto vIdxLocal1 = i[1];
                const auto directionNumber = i[2];
                setVerticesAndElements(vIdxLocal0, vIdxLocal1, directionProbability[directionNumber]);
            }
        }

        if(elementSet.empty())
        DUNE_THROW(Dune::GridError, "Trying to create pore network with zero elements!");

        // make the actual porenetwork grid
        Dune::GridFactory<Grid> factory;
        for(auto&& vertex : vertexSet)
            factory.insertVertex(vertex);
        for(auto&& element : elementSet)
#if DUNE_VERSION_NEWER(DUNE_COMMON,2,7)
            factory.insertElement(Dune::GeometryTypes::cube(1), {element.first, element.second});
#else
            factory.insertElement(Dune::GeometryType(1), {element.first, element.second});
#endif

        gridPtr() = std::shared_ptr<Grid>(factory.createGrid());
        gridData_ = std::make_shared<GridData>(gridPtr_, paramGroup_);

        if(getParamFromGroup<bool>(paramGroup_, "Grid.Sanitize", true))
            sanitizeGrid();
        else
            std::cout << "\nWARNING: Set Grid.Sanitize = true in order to remove insular patches of elements not connected to the boundary." << std::endl;

        gridData_->assignParameters();
    }

    /*!
     * \brief Returns the filename extension of a given filename
     */
    std::string getFileExtension(const std::string& fileName) const
    {
        std::size_t i = fileName.rfind('.', fileName.length());
        if (i != std::string::npos)
        {
            return(fileName.substr(i+1, fileName.length() - i));
        }
        else
        {
            DUNE_THROW(Dune::IOError, "Please provide and extension for your grid file ('"<< fileName << "')!");
        }
        return "";
    }

    /*!
     * \brief Returns a reference to the grid.
     */
    Grid& grid()
    {
        if(enableDgfGridPointer_)
            return *dgfGridPtr();
        else
            return *gridPtr();
    }

    std::shared_ptr<GridData> getGridData() const
    {
        if (!gridData_)
            DUNE_THROW(Dune::IOError, "No grid data available");

        return gridData_;
    }

    /*!
     * \brief Call loadBalance() function of the grid.
     */
    void loadBalance()
    {
        if (Dune::MPIHelper::getCollectiveCommunication().size() > 1)
        {
            // if we may have dgf parameters use load balancing of the dgf pointer
            if(enableDgfGridPointer_)
            {
                dgfGridPtr().loadBalance();
                // update the grid data
                gridData_ = std::make_shared<GridData>(dgfGridPtr(), paramGroup_);
            }

            else
                gridPtr()->loadBalance();
        }
    }

    /*!
     * \brief post-processing to remove insular groups of elements that are not connected to a Dirichlet boundary
     */
    void sanitizeGrid()
    {
        // evaluate the coordination numbers to check if all pores are connected to at least one throat
        gridData_->getCoordinationNumbers();

        // for dgf grid, copy the data to peristent containers
        if (enableDgfGridPointer_)
            gridData_->copyDgfData();

        // pruning -- remove elements not connected to a Dirichlet boundary (marker == 1)
        const auto pruningSeedIndices = getParamFromGroup<std::vector<int>>(paramGroup_, "Grid.PruningSeedIndices", std::vector<int>{1});
        const auto gridView = grid().leafGridView();
        std::vector<bool> elementIsConnected(gridView.size(0), false);

        auto boundaryIdx = [&](const auto& vertex)
        {
            if (enableDgfGridPointer_)
                return static_cast<int>(dgfGridPtr_.parameters(vertex)[gridData_->parameterIndex("PoreLabel")]);
            else
                return gridData_->boundaryFaceMarkerAtPos(vertex.geometry().center());
        };

        for (const auto& element : elements(gridView))
        {
            const auto eIdx = gridView.indexSet().index(element);
            if (elementIsConnected[eIdx])
                continue;

            // try to find a seed from which to start the search process
            bool isSeed = false;
            bool hasNeighbor = false;
            for (const auto& intersection : intersections(gridView, element))
            {
                auto vertex = element.template subEntity<1>(intersection.indexInInside());
                // seed found
                if (std::any_of(pruningSeedIndices.begin(), pruningSeedIndices.end(),
                               [&]( const int i ){ return i == boundaryIdx(vertex); }))
                {
                    isSeed = true;
                    // break;
                }

                if (intersection.neighbor())
                    hasNeighbor = true;
            }

            if (!hasNeighbor)
                continue;

            if (isSeed)
            {
                elementIsConnected[eIdx] = true;

                // use iteration instead of recursion here because the recursion can get too deep
                std::stack<Element> elementStack;
                elementStack.push(element);
                while (!elementStack.empty())
                {
                    auto e = elementStack.top();
                    elementStack.pop();
                    for (const auto& intersection : intersections(gridView, e))
                    {
                        if (!intersection.boundary())
                        {
                            auto outside = intersection.outside();
                            auto nIdx = gridView.indexSet().index(outside);
                            if (!elementIsConnected[nIdx])
                            {
                                elementIsConnected[nIdx] = true;
                                elementStack.push(outside);
                            }
                        }
                    }
                }
            }
        }

        if (std::none_of(elementIsConnected.begin(), elementIsConnected.end(), [](const bool i){return i;}))
            DUNE_THROW(Dune::InvalidStateException, "sanitize() deleted all elements! Check your boundary face indices. "
                << "If the problem persisits, add at least one of your boundary face indices to PruningSeedIndices");

        // remove the elements in the grid
        std::size_t numDeletedElements = 0;
        grid().preGrow();
        for (const auto& element : elements(gridView))
        {
            const auto eIdx = gridView.indexSet().index(element);
            if (!elementIsConnected[eIdx])
            {
                grid().removeElement(element);
                ++numDeletedElements;
            }
        }
        // triggers the grid growth process
        grid().grow();
        grid().postGrow();

        // resize the parameters for dgf grids
        if (enableDgfGridPointer_)
            gridData_->resizeParameterContainers();

        if (numDeletedElements > 0)
            std::cout << "\nDeleted " << numDeletedElements << " elements not connected to a boundary." << std::endl;
    }

protected:

    /*!
     * \brief Returns a reference to the grid pointer (std::shared_ptr<Grid>)
     */
    std::shared_ptr<Grid>& gridPtr()
    {
        if(!enableDgfGridPointer_)
            return gridPtr_;
        else
            DUNE_THROW(Dune::InvalidStateException, "You are using DGF. To get the grid pointer use method dgfGridPtr()!");
    }

    /*!
     * \brief Returns a reference to the DGF grid pointer (Dune::GridPtr<Grid>).
     */
    Dune::GridPtr<Grid>& dgfGridPtr()
    {
        if(enableDgfGridPointer_)
            return dgfGridPtr_;
        else
            DUNE_THROW(Dune::InvalidStateException, "The DGF grid pointer is only available if the grid was constructed with a DGF file!");
    }

    /*!
    * \brief A state variable if the DGF Dune::GridPtr has been enabled.
    *        It is always enabled if a DGF grid file was used to create the grid.
    */
    bool enableDgfGridPointer_ = false;

    std::shared_ptr<Grid> gridPtr_;
    Dune::GridPtr<Grid> dgfGridPtr_;

    std::shared_ptr<GridData> gridData_;

    std::string paramGroup_;

};

}

#endif
